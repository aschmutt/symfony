<?php

/* SensioTrainingBundle:Default:test.html.twig */
class __TwigTemplate_5eb2a8e84e2e59d1c98b843f83e381d66c4bf58ac626c3968e6bfc6bbf59c5ee extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "Neue Action <h2>";
        echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : $this->getContext($context, "name")), "html", null, true);
        echo "</h2>
";
    }

    public function getTemplateName()
    {
        return "SensioTrainingBundle:Default:test.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
