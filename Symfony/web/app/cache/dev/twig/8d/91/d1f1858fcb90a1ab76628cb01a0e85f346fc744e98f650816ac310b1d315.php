<?php

/* SensioTrainingBundle:Default:counter.html.twig */
class __TwigTemplate_8d91d1f1858fcb90a1ab76628cb01a0e85f346fc744e98f650816ac310b1d315 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<p>There are ";
        echo twig_escape_filter($this->env, (isset($context["usersOnline"]) ? $context["usersOnline"] : $this->getContext($context, "usersOnline")), "html", null, true);
        echo " users online.</p>";
    }

    public function getTemplateName()
    {
        return "SensioTrainingBundle:Default:counter.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  176 => 156,  19 => 1,  60 => 11,  55 => 20,  53 => 18,  49 => 16,  47 => 13,  42 => 11,  38 => 9,  36 => 6,  32 => 4,  29 => 3,);
    }
}
