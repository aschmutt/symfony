<?php

namespace Sensio\Bundle\TrainingBundle\Converter;

class CelsiusConverter {

    public function toFahrenheit($celsius) {
        return ((int)$celsius)*9/5+32;
    }
}