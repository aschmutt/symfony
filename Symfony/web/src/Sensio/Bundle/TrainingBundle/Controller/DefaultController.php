<?php

namespace Sensio\Bundle\TrainingBundle\Controller;

use Sensio\Bundle\TrainingBundle\Contact\Contact;
use Sensio\Bundle\TrainingBundle\Form\ContactType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/hello/{name}",name="training_hello")
     * @Template()
     */
    public function indexAction($name)
    {
        return array('name'=>$name);
    }

    /**
     * @Route("/test/{name}")
     * @Template()
     */
    public function testAction($name)
    {
        return array('name'=>$name);
    }

    /**
     * @Route(path="/color/{color}",name="training_color_set")
     * kein Template notwendig da redirect
     */
    public function setColorAction(Request $request, $color) {
        /*
        //Lösung mit Cookie:
        $cookie = new Cookie('color',$color, new \DateTime('+ 10 minutes'));
        $response = $this->redirect($this->generateUrl('training_color_show'));
        $response->headers->setCookie($cookie);
        return $response;
        */

        //Lösung mit Session
        $request->getSession()->set('color',$color);

        return $this->redirect($this->generateUrl('training_color_show'));
    }

    /**
     * @Route(path="/color",name="training_color_show")
     * @Template()
     */
    public function showColorAction(Request $request) {
        /*
        //Lösung mit Cookie:
        $color = $request->cookies->get('color','ff0000');
        return array('color'=>$color);
        */

        //Lösung mit Session
        $color = $request->getSession()->get('color','ff0000');
        return array('color'=>$color);
    }

    /**
    * @Template()
     */
    public function counterAction(){
        $this->bundesliga();
        return array(
            'usersOnline' => mt_rand(1000,10000)
        );
    }

    public function bundesliga() {


        echo ' <style>
.blTicker {font-family:"Trebuchet MS",Verdana, Arial, sans-serif; font-size:12px;line-height:140%; }
.blTicker th {background:#6D8D9F; text-align:left; border:0; color:#fff;padding:2px 5px 2px 5px;}
.blTicker td {padding:2px 5px 2px 5px; margin:0; border:0;}
.blTicker td.dark {background:#D1DADF; }
.blTicker td.bright {background:#E0E8EF; }
.blTicker a {color:#ccc; text-decoration:none;}
.blTicker a:hover {color:#fff; text-decoration:none;}
.blTicker .foot {text-align:center;background:#6D8D9F; color:#ff0000}
.blTicker .foot small { color:#222}
.blTicker small {color:#777;}
</style>'."\r\n";


// http://www.kicker.de/news/live-news/rss-feed/371410/artikel_live-rss.html
// rss.kicker.de/live/2bundesliga

        $rss_file = '';
        $fp = fsockopen ('rss.kicker.de', 80, $errno, $errstr, 5);
        if ($fp) {
            fputs ($fp, "GET /live/bundesliga HTTP/1.0\r\nHost: rss.kicker.de\r\n\r\n");
            while (!feof($fp))  {
                $rss_file .= fgets($fp);
            }
            fclose($fp);
        } else  {echo('RSS Ladefehler :(');}
        $rss_zeilen = array ( "title", "description",  "category", "pubDate" );
        $rss_array = explode ( "<item>", $rss_file );
        echo '<!-- Bundesliga-Ticker von www.online-fussballmanager.eu --><table class="blTicker"><tr><th> ';
        $lineC = 0;
        foreach ( $rss_array as $string ) {
            $arrData =array();
            foreach ( $rss_zeilen as $zeile ) {
                preg_match_all ( "|<$zeile>(.*)</$zeile>|Usim", $string, $preg_match );
                $$zeile = $preg_match [1] [0];
                $arrData[$zeile] = $$zeile;
            }
            $tickerClass = round($lineC/2) == $lineC/2 ? 'dark' : 'bright';
            if (stripos($arrData['description'], 'kicker'  ) !== false) {
                echo "". $arrData['category'] .' </th></tr>'."\r\n";
            } else {
                echo '<tr><td class="'.$tickerClass.'">'.htmlentities($arrData['title']) .' <br><small>'.str_ireplace(array('+0100','+0200', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'), array('', '', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag', 'Sonntag', 'Januar', 'Februar', 'M&auml;rz', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember'), $arrData['pubDate']). "</small></td></tr>\r\n";
            }
            $lineC++;
        }
        echo '<tr><td class="foot"><small> powered by <a href="http://www.online-fussballmanager.eu" target="_blank">online-fu&szlig;ballmanager.eu</a></small></td></tr></table>';
    }

    /**
     * @Route(path="/contact/{danke}",name="training_contact")
     * @Template
     */
    public function contactAction(Request $request,$danke){
        $contact = new Contact();
        $type = new ContactType();
        $form = $this->createForm($type,$contact);

        $form->handleRequest($request);

        if ($form->isValid()) {

            $message = \Swift_Message::newInstance()
                ->setFrom($contact->getSender())
                ->setSubject($contact->getSubject())
                ->setBody($contact->getMessage())
                ->setTo('andrea@schmutt.de')
                ;

            $this->get('mailer')->send($message);

            return $this->redirect($this->generateUrl('training_contact',array('danke'=>1)));
        }

        return array(
            'form'=>$form->createView(),
            'danke'=>$danke
        );


    }
}
