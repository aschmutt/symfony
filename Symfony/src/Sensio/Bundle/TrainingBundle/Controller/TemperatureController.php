<?php

namespace Sensio\Bundle\TrainingBundle\Controller;

use Sensio\Bundle\TrainingBundle\Converter\CelsiusConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class TemperatureController extends Controller {

    /**
     * @Route(
     *      path="convert/{celsius}/fahrenheit.{_format}",
     *      name="training_celsius",
     *      methods="GET",
     *      requirements={
                "celsius"="\d+",
     *          "_format"="html|xml"
     *      }
     * )
     * @Template()
     */
    public function celsiusAction($celsius){

        $converter = new CelsiusConverter();

        return array(
            'celsius' => $celsius,
            'fahrenheit' => $converter->toFahrenheit($celsius)
        );
    }
}