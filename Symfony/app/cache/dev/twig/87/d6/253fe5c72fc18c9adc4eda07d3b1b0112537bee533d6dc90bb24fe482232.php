<?php

/* SensioTrainingBundle:Temperature:celsius.xml.twig */
class __TwigTemplate_87d6253fe5c72fc18c9adc4eda07d3b1b0112537bee533d6dc90bb24fe482232 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<temperature>
    <celsius>";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["celsius"]) ? $context["celsius"] : $this->getContext($context, "celsius")), "html", null, true);
        echo "</celsius>
    <fahrenheit>";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["fahrenheit"]) ? $context["fahrenheit"] : $this->getContext($context, "fahrenheit")), "html", null, true);
        echo "</fahrenheit>
</temperature>";
    }

    public function getTemplateName()
    {
        return "SensioTrainingBundle:Temperature:celsius.xml.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  26 => 3,  22 => 2,  19 => 1,);
    }
}
