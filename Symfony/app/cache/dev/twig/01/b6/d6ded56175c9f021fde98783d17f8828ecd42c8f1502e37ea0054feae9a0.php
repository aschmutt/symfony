<?php

/* SensioTrainingBundle:Temperature:celsius.html.twig */
class __TwigTemplate_01b6d6ded56175c9f021fde98783d17f8828ecd42c8f1502e37ea0054feae9a0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("SensioTrainingBundle::layout.html.twig");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SensioTrainingBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "<p>Celsius: ";
        echo twig_escape_filter($this->env, (isset($context["celsius"]) ? $context["celsius"] : $this->getContext($context, "celsius")), "html", null, true);
        echo "</p>
    <p>Fahrenheit: ";
        // line 5
        echo twig_escape_filter($this->env, (isset($context["fahrenheit"]) ? $context["fahrenheit"] : $this->getContext($context, "fahrenheit")), "html", null, true);
        echo "</p>
";
    }

    public function getTemplateName()
    {
        return "SensioTrainingBundle:Temperature:celsius.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  36 => 5,  31 => 4,  28 => 3,);
    }
}
