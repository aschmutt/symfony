<?php

/* SensioTrainingBundle:Default:index.html.twig */
class __TwigTemplate_86f5cc1943d71e31a412f279d49d683794985b4f3b3037a1655e398b4b95c667 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("SensioTrainingBundle::layout.html.twig");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SensioTrainingBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "Super Code ausm Twig <h2>";
        echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : $this->getContext($context, "name")), "html", null, true);
        echo "</h2>

    <p>Say hello to <a href=\"";
        // line 6
        echo $this->env->getExtension('routing')->getPath("training_hello", array("name" => "SensioAndrea"));
        echo "\">Sensio</a></p>

    <p>Absolute URL:  ";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getUrl("training_hello", array("name" => (isset($context["name"]) ? $context["name"] : $this->getContext($context, "name")))), "html", null, true);
        echo "</p>

";
    }

    public function getTemplateName()
    {
        return "SensioTrainingBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 8,  37 => 6,  31 => 4,  28 => 3,);
    }
}
