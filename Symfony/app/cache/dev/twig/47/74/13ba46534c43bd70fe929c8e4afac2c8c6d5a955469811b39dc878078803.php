<?php

/* SensioTrainingBundle::menu.html.twig */
class __TwigTemplate_477413ba46534c43bd70fe929c8e4afac2c8c6d5a955469811b39dc878078803 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<style>

    #menu-bar {
        width: 95%;
        margin: 0px 0px 0px 0px;
        padding: 6px 6px 4px 6px;
        height: 40px;
        line-height: 100%;
        border-radius: 24px;
        -webkit-border-radius: 24px;
        -moz-border-radius: 24px;
        box-shadow: 2px 2px 3px #666666;
        -webkit-box-shadow: 2px 2px 3px #666666;
        -moz-box-shadow: 2px 2px 3px #666666;
        background: #8B8B8B;
        background: linear-gradient(top,  #A9A9A9,  #7A7A7A);
        background: -ms-linear-gradient(top,  #A9A9A9,  #7A7A7A);
        background: -webkit-gradient(linear, left top, left bottom, from(#A9A9A9), to(#7A7A7A));
        background: -moz-linear-gradient(top,  #A9A9A9,  #7A7A7A);
        border: solid 1px #6D6D6D;
        position:relative;
        z-index:999;
    }
    #menu-bar li {
        margin: 0px 0px 6px 0px;
        padding: 0px 6px 0px 6px;
        float: left;
        position: relative;
        list-style: none;
    }
    #menu-bar a {
        font-weight: bold;
        font-family: arial;
        font-style: normal;
        font-size: 12px;
        color: #E7E5E5;
        text-decoration: none;
        display: block;
        padding: 6px 20px 6px 20px;
        margin: 0;
        margin-bottom: 6px;
        border-radius: 10px;
        -webkit-border-radius: 10px;
        -moz-border-radius: 10px;
        text-shadow: 2px 2px 3px #000000;
    }
    #menu-bar li ul li a {
        margin: 0;
    }
    #menu-bar .active a, #menu-bar li:hover > a {
        background: #0399D4;
        background: linear-gradient(top,  #EBEBEB,  #A1A1A1);
        background: -ms-linear-gradient(top,  #EBEBEB,  #A1A1A1);
        background: -webkit-gradient(linear, left top, left bottom, from(#EBEBEB), to(#A1A1A1));
        background: -moz-linear-gradient(top,  #EBEBEB,  #A1A1A1);
        color: #444444;
        -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, .2);
        -moz-box-shadow: 0 1px 1px rgba(0, 0, 0, .2);
        box-shadow: 0 1px 1px rgba(0, 0, 0, .2);
        text-shadow: 2px 2px 3px #FFFFFF;
    }
    #menu-bar ul li:hover a, #menu-bar li:hover li a {
        background: none;
        border: none;
        color: #666;
        -box-shadow: none;
        -webkit-box-shadow: none;
        -moz-box-shadow: none;
    }
    #menu-bar ul a:hover {
        background: #0399D4 !important;
        background: linear-gradient(top,  #04ACEC,  #0186BA) !important;
        background: -ms-linear-gradient(top,  #04ACEC,  #0186BA) !important;
        background: -webkit-gradient(linear, left top, left bottom, from(#04ACEC), to(#0186BA)) !important;
        background: -moz-linear-gradient(top,  #04ACEC,  #0186BA) !important;
        color: #FFFFFF !important;
        border-radius: 0;
        -webkit-border-radius: 0;
        -moz-border-radius: 0;
        text-shadow: 2px 2px 3px #FFFFFF;
    }
    #menu-bar ul {
        background: #DDDDDD;
        background: linear-gradient(top,  #FFFFFF,  #CFCFCF);
        background: -ms-linear-gradient(top,  #FFFFFF,  #CFCFCF);
        background: -webkit-gradient(linear, left top, left bottom, from(#FFFFFF), to(#CFCFCF));
        background: -moz-linear-gradient(top,  #FFFFFF,  #CFCFCF);
        display: none;
        margin: 0;
        padding: 0;
        width: 185px;
        position: absolute;
        top: 40px;
        left: 0;
        border: solid 1px #B4B4B4;
        border-radius: 10px;
        -webkit-border-radius: 10px;
        -moz-border-radius: 10px;
        -webkit-box-shadow: 2px 2px 3px #222222;
        -moz-box-shadow: 2px 2px 3px #222222;
        box-shadow: 2px 2px 3px #222222;
    }
    #menu-bar li:hover > ul {
        display: block;
    }
    #menu-bar ul li {
        float: none;
        margin: 0;
        padding: 0;
    }
    #menu-bar ul a {
        padding:10px 0px 10px 15px;
        color:#424242 !important;
        font-size:12px;
        font-style:normal;
        font-family:arial;
        font-weight: normal;
        text-shadow: 2px 2px 3px #FFFFFF;
    }
    #menu-bar ul li:first-child > a {
        border-top-left-radius: 10px;
        -webkit-border-top-left-radius: 10px;
        -moz-border-radius-topleft: 10px;
        border-top-right-radius: 10px;
        -webkit-border-top-right-radius: 10px;
        -moz-border-radius-topright: 10px;
    }
    #menu-bar ul li:last-child > a {
        border-bottom-left-radius: 10px;
        -webkit-border-bottom-left-radius: 10px;
        -moz-border-radius-bottomleft: 10px;
        border-bottom-right-radius: 10px;
        -webkit-border-bottom-right-radius: 10px;
        -moz-border-radius-bottomright: 10px;
    }
    #menu-bar:after {
        content: \".\";
        display: block;
        clear: both;
        visibility: hidden;
        line-height: 0;
        height: 0;
    }
    #menu-bar {
        display: inline-block;
    }
    html[xmlns] #menu-bar {
        display: block;
    }
    * html #menu-bar {
        height: 1%;
    }

</style>

<ul id=\"";
        // line 156
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")), "html", null, true);
        echo "\" class=\"menu\">
    <li><a href=\"";
        // line 157
        echo $this->env->getExtension('routing')->getPath("training_hello", array("name" => "SensioBootcamp"));
        echo "\">Hello</a></li>
    <li><a href=\"";
        // line 158
        echo $this->env->getExtension('routing')->getPath("training_color_show", array("color" => "0000ff"));
        echo "\">Color</a></li>
    <li><a href=\"";
        // line 159
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("training_celsius", array("celsius" => 22, "_format" => "html")), "html", null, true);
        echo "\">Celsius</a></li>
    <li><a href=\"";
        // line 160
        echo $this->env->getExtension('routing')->getPath("training_contact", array("danke" => 0));
        echo "\">Form</a></li>
</ul>";
    }

    public function getTemplateName()
    {
        return "SensioTrainingBundle::menu.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  192 => 160,  188 => 159,  184 => 158,  180 => 157,  176 => 156,  19 => 1,);
    }
}
