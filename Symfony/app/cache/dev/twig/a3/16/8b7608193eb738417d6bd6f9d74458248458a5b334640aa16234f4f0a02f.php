<?php

/* SensioTrainingBundle:Default:contact.html.twig */
class __TwigTemplate_a3168b7608193eb738417d6bd6f9d74458248458a5b334640aa16234f4f0a02f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("SensioTrainingBundle::layout.html.twig");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SensioTrainingBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        if (((isset($context["danke"]) ? $context["danke"] : $this->getContext($context, "danke")) == 1)) {
            echo " DANKE!!!!
        ";
        }
        // line 6
        echo "
   <form action=\"\" method=\"post\" novalidate=\"novalidate\" >
       ";
        // line 8
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
       <button type=\"submit\">Send</button>
   </form>

";
    }

    public function getTemplateName()
    {
        return "SensioTrainingBundle:Default:contact.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  41 => 8,  37 => 6,  31 => 4,  28 => 3,);
    }
}
